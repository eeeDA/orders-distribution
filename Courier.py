# import numpy as np
# import random
# from itertools import permutations
# import matplotlib.pyplot as plt
# from tqdm import tqdm
# import scipy.stats as sps
# import requests
# import xml.etree.ElementTree as ET
# from tqdm import tqdm
# import pandas as pd
# from datetime import datetime

# import plotly.express as px
# import plotly.graph_objects as go
# from pypolyline.cutil import encode_coordinates, decode_polyline

# class Courier:
#     def __init__(self, id, location=SPOT_LOCATION, max_orders=3):
#         self.id = id
#         self.location = location
#         self.orders = []
#         self.status = "free"
#         self.available_at = 0
#         self.max_orders = max_orders
#         self.velocity = 30
#         self.optimal_route_log = []
#         self.route_coordinates = []
#         self.assigned_orders = []

#     def assign_orders(self, orders, current_time):
#         if self.status == "free" and len(orders) <= self.max_orders and len(orders) > 0:
#             route, sorted_orders, route_coordinates = self.calculate_optimal_route(orders)
#             self.status = "busy"
#             cumulative_travel_time = 0
#             travel_times = []
#             current_location = self.location

#             # Calculate travel time for each segment of the route
#             for order in sorted_orders:
#                 _, travel_time = self.calculate_duration_and_nodes([current_location, order.destination_location])
#                 cumulative_travel_time += travel_time
#                 travel_times.append(travel_time)
#                 current_location = order.destination_location  # Update current location to this order's destination

#             # Update orders with assigned and delivery times
#             cumulative_time = 0
#             for order, travel_time in zip(sorted_orders, travel_times):
#                 cumulative_time += travel_time
#                 order.time_assigned = current_time
#                 order.delivery_time = current_time + cumulative_time
#                 order.is_delivered = True

#             _, return_time = self.calculate_duration_and_nodes(
#                 [sorted_orders[-1].destination_location, self.location]
#             )
#             self.available_at = current_time + cumulative_travel_time + return_time

#             self.route_coordinates = route_coordinates
#             self.assigned_orders = sorted_orders
#             return sorted_orders, route_coordinates  # Return the sorted list of orders as per route
#         return [], []

#     def calculate_optimal_route(self, orders):
#         if not orders:
#             return [], [], []
        
#         # Generate all permutations of orders and calculate the route with the minimum travel distance
#         min_duration = float('inf')
#         best_permutation = []
#         best_route = []
#         route_coordinates = []
#         for perm in permutations(orders):
#             route = [self.location] + list(point.destination_location for point in perm) + [self.location]
#             # print(route)
#             route_coordinates, duration = Courier.calculate_duration_and_nodes(route)

#             if duration < min_duration:
#                 min_duration = duration
#                 best_permutation = perm
#                 best_route = route
#                 best_route_coordinates = route_coordinates

#         self.optimal_route_log.append(best_route)
#         return best_route, list(best_permutation), best_route_coordinates

#     @staticmethod
#     def calculate_duration_and_nodes(route):

#         coordinates_string = ';'.join([f'{location[0]},{location[1]}' for location in route])
#         url = f'http://router.project-osrm.org/route/v1/driving/{coordinates_string}?alternatives=false&annotations=nodes'

#         headers = { 'Content-type': 'application/json'}
#         r = requests.get(url, headers = headers)
#         routejson = r.json()
#         # print("Calling API ...:", r.status_code) # Status Code 200 is success
#         # print(url)
        
#         route_coordinates = decode_polyline(str.encode(routejson['routes'][0]['geometry']), 5)
#         duration = routejson['routes'][0]['duration']

#         return route_coordinates, duration

#     def get_route_coordinates(self, route_nodes):

#         route_list = []
#         for i in range(0, len(route_nodes)):
#             if i % 3==1:
#                 route_list.append(route_nodes[i])
        
#         coordinates = []
        
#         for node in tqdm(route_list):
#             try:
#                 url = 'https://api.openstreetmap.org/api/0.6/node/' + str(node)
#                 headers = { 'Content-type': 'application/json'}
#                 r = requests.get(url, headers = headers)
#                 myroot = ET.fromstring(r.text)
#                 for child in myroot:
#                     lat, long = child.attrib['lat'], child.attrib['lon']
#                     coordinates.append((lat, long))
#             except:
#                 continue

#         print(coordinates)
#         return coordinates

#     def update_status_and_orders(self, current_time):

#         for order in self.assigned_orders.copy():

#             if order.delivery_time < current_time:
#                 self.assigned_orders.remove(order)
                
#         if current_time >= self.available_at:
#             assert len(self.assigned_orders) == 0, f'Courier {self.id} has undelivered orders: {self.assigned_orders}. Current time is {current_time}'
#             self.status = "free"
#             # self.location = (0, 0)