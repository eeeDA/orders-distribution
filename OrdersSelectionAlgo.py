# import random
# import numpy as np
# from Courier import Courier

# def greedy_order_selection(courier, waiting_orders):
#     selected_orders = []
#     current_location = courier.location

#     remaining_orders = waiting_orders.copy()

#     while remaining_orders and len(selected_orders) < courier.max_orders:
#         next_order = min(remaining_orders, key=lambda order: calculate_duration(current_location, order.destination_location))
#         remaining_orders.remove(next_order)
#         selected_orders.append(next_order)
#         current_location = next_order.destination_location

#     return selected_orders

# def calculate_duration(location1, location2):
#     _, duration = Courier.calculate_duration_and_nodes([location1, location2])
#     return duration


# def simulated_annealing_order_selection(courier, waiting_orders):
#     if not waiting_orders:
#         return []

#     def calculate_total_duration(orders):
#         current_location = courier.location
#         total_duration = 0
#         for order in orders:
#             duration = calculate_duration(current_location, order.destination_location)
#             total_duration += duration
#             current_location = order.destination_location
#         return total_duration

#     def swap_orders(orders):
#         if len(orders) < 2:
#             return orders
#         new_orders = orders[:]
#         i, j = random.sample(range(len(new_orders)), 2)
#         new_orders[i], new_orders[j] = new_orders[j], new_orders[i]
#         return new_orders

#     current_orders = random.sample(waiting_orders, min(len(waiting_orders), courier.max_orders))
#     current_duration = calculate_total_duration(current_orders)
#     best_orders = current_orders
#     best_duration = current_duration
#     temperature = 100.0
#     cooling_rate = 0.5

#     while temperature > 1:
#         new_orders = swap_orders(current_orders)
#         new_duration = calculate_total_duration(new_orders)

#         if new_duration < current_duration or random.uniform(0, 1) < np.exp((current_duration - new_duration) / temperature):
#             current_orders = new_orders
#             current_duration = new_duration

#             if new_duration < best_duration:
#                 best_orders = new_orders
#                 best_duration = new_duration

#         temperature *= cooling_rate

#     return best_orders


# def genetic_algorithm_order_selection(courier, waiting_orders, population_size=50, generations=5, mutation_rate=0.1):
#     import random

#     if not waiting_orders:
#         return []

#     def calculate_total_duration(orders):
#         current_location = courier.location
#         total_duration = 0
#         for order in orders:
#             duration = calculate_duration(current_location, order.destination_location)
#             total_duration += duration
#             current_location = order.destination_location
#         return total_duration

#     def fitness(orders):
#         return 1 / calculate_total_duration(orders)

#     def mutate(orders):
#         new_orders = orders[:]
#         if len(new_orders) > 1:
#             i, j = random.sample(range(len(new_orders)), 2)
#             new_orders[i], new_orders[j] = new_orders[j], new_orders[i]
#         return new_orders

#     # Ensure the population size does not exceed the number of possible orders
#     population_size = min(population_size, len(waiting_orders))

#     # Create initial population
#     population = [random.sample(waiting_orders, min(len(waiting_orders), courier.max_orders)) for _ in range(population_size)]

#     for _ in range(generations):
#         population = sorted(population, key=fitness, reverse=True)
#         next_population = population[:population_size // 2]

#         while len(next_population) < population_size:
#             if len(next_population) < 2:
#                 next_population.extend(population[:2 - len(next_population)])
#                 continue
#             parents = random.sample(next_population, 2)
#             if len(parents[0]) > 1 and len(parents[1]) > 1:
#                 crossover_point = random.randint(1, min(len(parents[0]), len(parents[1])) - 1)
#                 child = parents[0][:crossover_point] + [order for order in parents[1] if order not in parents[0][:crossover_point]]
#                 if random.uniform(0, 1) < mutation_rate:
#                     child = mutate(child)
#                 next_population.append(child)
#             else:
#                 next_population.append(parents[0] if random.uniform(0, 1) < 0.5 else parents[1])

#         population = next_population

#     best_orders = max(population, key=fitness)
#     return best_orders

# def get_first_n_orders(courier, waiting_orders, n_orders):
#     return waiting_orders[:n_orders]

# def wait_for_n_orders(courier, waiting_orders, n_orders):
#     if len(waiting_orders) < n_orders:
#         return []
#     else:
#         return waiting_orders[:n_orders]


# def mean_orders(couriers, waiting_orders):
#     free_couriers_count = 0
#     courier_max_orders = couriers[0].max_orders
#     for courier in couriers:
#         free_couriers_count += (courier.status == 'free')
#     n_orders = min(courier_max_orders, (len(waiting_orders) + free_couriers_count - 1) // free_couriers_count)
#     return waiting_orders[:n_orders]