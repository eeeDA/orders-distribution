import plotly.express as px
import plotly.graph_objects as go

def create_style_dict():
    fig_dict = {
        "data": [],
        "layout": {},
        "frames": []
    }
    
    fig_dict['layout']['height'] = 800
    fig_dict['layout']['width'] = 1200
    fig_dict['layout']['mapbox_style'] = 'open-street-map'



    fig_dict["layout"]["updatemenus"] = [
        dict(
            type="buttons",
            buttons=[
                dict(
                    label="Play",
                    method="animate",
                    args=[None, {"frame": {"duration": 500}}]
                ),
                dict(
                    label="Pause",
                    method="animate",
                    args=[
                        None,
                        {
                            "frame": {"duration": 0, "redraw": False},
                            "mode": "immediate",
                            "transition": {"duration": 0}
                        }
                    ],
                )
            ]
        )
    ]

    return fig_dict


def prepare_data(states_log, courier_colors={0: 'blue', 1: 'black', 2: 'brown'}):

    assigned_orders_long = [[order.destination_location[0] for order in state['assigned_orders']] for state in states_log]
    assigned_orders_lat = [[order.destination_location[1] for order in state['assigned_orders']] for state in states_log]

    delivered_orders_long = [[order.destination_location[0] for order in state['delivered_orders']] for state in states_log]
    delivered_orders_lat = [[order.destination_location[1] for order in state['delivered_orders']] for state in states_log]

    waiting_orders_long = [[order.destination_location[0] for order in state['waiting_orders']] for state in states_log]
    waiting_orders_lat = [[order.destination_location[1] for order in state['waiting_orders']] for state in states_log]

    couriers_routes_long = [{
        courier_id: [point[0] for point in route] for courier_id, route in state['courier_routes'].items()
    } for state in states_log]

    couriers_routes_lat = [{
        courier_id: [point[1] for point in route] for courier_id, route in state['courier_routes'].items()
    } for state in states_log]

    start_data = [
        go.Scattermapbox(lat=assigned_orders_lat[0], lon=assigned_orders_long[0], mode="markers", marker=dict(size=10, color="orange"), visible=False, 
                        name='assigned_orders'),
        go.Scattermapbox(lat=waiting_orders_lat[0], lon=waiting_orders_long[0], mode="markers", marker=dict(size=10, color="red"), visible=False,
                        name='waiting_orders'),
        go.Scattermapbox(lat=delivered_orders_lat[0], lon=delivered_orders_long[0], mode="markers", marker=dict(size=10, color="green"), visible=False,
                        name='delivered_orders')
    ]

    for courier_id in couriers_routes_long[0].keys():

        long = couriers_routes_long[0][courier_id]
        lat = couriers_routes_lat[0][courier_id]

        start_data.append(go.Scattermapbox(lat=lat, lon=long, mode="markers+lines", line=dict(width=2, color=courier_colors[courier_id]), visible=False, name=f'courier {courier_id}'))
        
    frames = []

    for i in range(len(states_log)):

        state_data = [
            go.Scattermapbox(lat=assigned_orders_lat[i], lon=assigned_orders_long[i], mode="markers", marker=dict(size=10, color="orange"), visible=True,
                            name='assigned_orders'),
            go.Scattermapbox(lat=waiting_orders_lat[i], lon=waiting_orders_long[i], mode="markers", marker=dict(size=10, color="red"), visible=True,
                            name='waiting_orders'),
            go.Scattermapbox(lat=delivered_orders_lat[i], lon=delivered_orders_long[i], mode="markers", marker=dict(size=10, color="green"), visible=True,
                            name='delivered_orders')
        ]

        for courier_id in couriers_routes_long[i].keys():

            long = couriers_routes_long[i][courier_id]
            lat = couriers_routes_lat[i][courier_id]

            # print(long, lat)
    
            state_data.append(go.Scattermapbox(lat=lat, lon=long, mode="markers+lines", line=dict(width=2, color=courier_colors[courier_id]), visible=True, name=f'courier {courier_id}'))

        frames.append(go.Frame(data=state_data))

    return start_data, frames


def plot_states_log(states_log):

    start_data, frames = prepare_data(states_log)
    fig_dict = create_style_dict()
    
    fig_dict['data'] = start_data
    fig_dict['frames'] = frames

    fig = go.Figure(fig_dict)
    # fig.add_traces(go.Scattergeo(lat=[55.911160, 55.959496], lon=[37.472507, 37.537339]))
    fig.show()