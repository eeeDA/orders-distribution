FROM python:3.11.9-slim-bullseye


# WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

# RUN  python3 run.py