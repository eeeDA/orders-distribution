import numpy as np
import random
from itertools import permutations
import matplotlib.pyplot as plt
from tqdm import tqdm
import scipy.stats as sps
import requests
import xml.etree.ElementTree as ET
from tqdm import tqdm
import pandas as pd
from datetime import datetime, timedelta

import plotly.express as px
import plotly.graph_objects as go
from pypolyline.cutil import encode_coordinates, decode_polyline


SPOT_LOCATION = (37.515724, 55.922349)
LONGITUDE_LIMITS = (37.482507, 37.527339)
LATITUDE_LIMITS = (55.921160, 55.949496)


class Courier:
    def __init__(self, id, location=SPOT_LOCATION, max_orders=3):
        self.id = id
        self.location = location
        self.orders = []
        self.status = "free"
        self.available_at = 0
        self.max_orders = max_orders
        self.velocity = 30
        self.optimal_route_log = []
        self.route_coordinates = []
        self.assigned_orders = []

    def assign_orders(self, orders, current_time):
        if self.status == "free" and len(orders) <= self.max_orders and len(orders) > 0:
            route, sorted_orders, route_coordinates = self.calculate_optimal_route(orders)
            self.status = "busy"
            cumulative_travel_time = 0
            travel_times = []
            current_location = self.location

            # Calculate travel time for each segment of the route
            for order in sorted_orders:
                _, travel_time = self.calculate_duration_and_nodes([current_location, order.destination_location])
                cumulative_travel_time += travel_time
                travel_times.append(travel_time)
                current_location = order.destination_location  # Update current location to this order's destination

            # Update orders with assigned and delivery times
            cumulative_time = 0
            for order, travel_time in zip(sorted_orders, travel_times):
                cumulative_time += travel_time
                order.time_assigned = current_time
                order.delivery_time = current_time + cumulative_time
                order.is_delivered = True

            _, return_time = self.calculate_duration_and_nodes(
                [sorted_orders[-1].destination_location, self.location]
            )
            self.available_at = current_time + cumulative_travel_time + return_time

            self.route_coordinates = route_coordinates
            self.assigned_orders = sorted_orders
            return sorted_orders, route_coordinates  # Return the sorted list of orders as per route
        return [], []

    def calculate_optimal_route(self, orders):
        if not orders:
            return [], [], []
        
        # Generate all permutations of orders and calculate the route with the minimum travel distance
        min_duration = float('inf')
        best_permutation = []
        best_route = []
        route_coordinates = []
        for perm in permutations(orders):
            route = [self.location] + list(point.destination_location for point in perm) + [self.location]
            # print(route)
            route_coordinates, duration = Courier.calculate_duration_and_nodes(route)

            if duration < min_duration:
                min_duration = duration
                best_permutation = perm
                best_route = route
                best_route_coordinates = route_coordinates

        self.optimal_route_log.append(best_route)
        return best_route, list(best_permutation), best_route_coordinates

    @staticmethod
    def calculate_duration_and_nodes(route):

        coordinates_string = ';'.join([f'{location[0]},{location[1]}' for location in route])
        url = f'http://router.project-osrm.org/route/v1/driving/{coordinates_string}?alternatives=false&annotations=nodes'

        headers = { 'Content-type': 'application/json'}
        r = requests.get(url, headers = headers)
        routejson = r.json()
        # print("Calling API ...:", r.status_code) # Status Code 200 is success
        # print(url)
        
        route_coordinates = decode_polyline(str.encode(routejson['routes'][0]['geometry']), 5)
        duration = routejson['routes'][0]['duration']

        return route_coordinates, duration

    def get_route_coordinates(self, route_nodes):

        route_list = []
        for i in range(0, len(route_nodes)):
            if i % 3==1:
                route_list.append(route_nodes[i])
        
        coordinates = []
        
        for node in tqdm(route_list):
            try:
                url = 'https://api.openstreetmap.org/api/0.6/node/' + str(node)
                headers = { 'Content-type': 'application/json'}
                r = requests.get(url, headers = headers)
                myroot = ET.fromstring(r.text)
                for child in myroot:
                    lat, long = child.attrib['lat'], child.attrib['lon']
                    coordinates.append((lat, long))
            except:
                continue

        print(coordinates)
        return coordinates

    def update_status_and_orders(self, current_time):

        for order in self.assigned_orders.copy():

            if order.delivery_time < current_time:
                self.assigned_orders.remove(order)
                
        if current_time >= self.available_at:
            assert len(self.assigned_orders) == 0, f'Courier {self.id} has undelivered orders: {self.assigned_orders}. Current time is {current_time}'
            self.status = "free"
            # self.location = (0, 0)



class Order:
    def __init__(self, id, destination_location, creation_time, undelivered_penalty, time_shift=None):
        self.id = id
        self.destination_location = destination_location
        self.creation_time = creation_time
        self.time_assigned = None
        self.delivery_time = None
        self.is_delivered = False
        self.penalty = undelivered_penalty
        self.shift_time = time_shift
        _, self.least_possible_penalty = Courier.calculate_duration_and_nodes([SPOT_LOCATION, self.destination_location])

    
    def calculate_penalty(self):
        if self.is_delivered:
            delay = self.delivery_time - self.creation_time
            self.penalty = delay
        else:
            self.peanlty = 10 * self.least_possible_penalty

    def __repr__(self):
        if self.shift_time is None:
            return f'{self.id}: creation time - {self.creation_time}, assignment time - {self.time_assigned}, delivery time - {self.delivery_time} \n'
        else: 
            creation_time = self.shift_time + timedelta(seconds=self.creation_time)
            time_assigned = self.shift_time + timedelta(seconds=self.time_assigned)
            delivery_time = self.shift_time + timedelta(seconds=self.delivery_time)
            return f'{self.id}: creation time - {creation_time}, assignment time - {time_assigned}, delivery time - {delivery_time} \n'

    def __str__(self):
        return self.__repr__()


def is_point_in_polygon(x, y, polygon):

    num_vertices = len(polygon)
    inside = False

    px, py = polygon[0]
    for i in range(num_vertices + 1):
        qx, qy = polygon[i % num_vertices]
        if y > min(py, qy):
            if y <= max(py, qy):
                if x <= max(px, qx):
                    if py != qy:
                        intersect_x = (y - py) * (qx - px) / (qy - py) + px
                    if px == qx or x <= intersect_x:
                        inside = not inside
        px, py = qx, qy

    return inside


def calculate_average_distance(points):

    points = np.array(points)
    n = len(points)
    
    # Создаем матрицу расстояний
    distances = np.sqrt(((points[:, np.newaxis] - points[np.newaxis, :]) ** 2).sum(axis=2))
    
    # Суммируем все расстояния и делим на количество пар
    total_distance = np.sum(distances) / 2  # Каждое расстояние учтено дважды
    count = n * (n - 1) / 2  # Количество уникальных пар точек
    
    return total_distance / count if count > 0 else 0

class Simulation:

    def __init__(
        self, 
        n_orders=None, 
        n_couriers=3, 
        lam=None, 
        polygon_boarders=None, 
        polygon_boarders_file=None, 
        selection_algorithm='greedy', 
        precomputed_orders=None,
        precomputed_orders_file=None,
        sample_similar=False
    ):
        self.n_orders = n_orders
        self.couriers = [Courier(i) for i in range(n_couriers)]
        self.lam = lam
        self.all_orders = []
        self.delivered_orders = []
        self.assigned_orders = []
        self.waiting_orders = []
        self.total_penalty = 0
        self.states_log = []
        self.sample_similar = sample_similar

        # assert (polygon_boarders is None) ^ (polygon_boarders_file is None)
        self.polygon_boarders = None
        if polygon_boarders is not None:
            self.polygon_boarders = polygon_boarders
        elif polygon_boarders_file is not None:
            self.polygon_boarders = np.array(pd.read_csv(polygon_boarders_file)[['lat', 'lon']])

        if self.polygon_boarders is not None:
            self.lat_lim = (np.min(self.polygon_boarders, axis=0), np.max(self.polygon_boarders, axis=0))
            self.long_lim = (np.min(self.polygon_boarders, axis=1), np.max(self.polygon_boarders, axis=1))

        self.selection_algorithm = selection_algorithm
        self.precomputed_orders = precomputed_orders
        if precomputed_orders_file is not None:
            self.process_precomputed_orders_from_file(precomputed_orders_file)

    def process_precomputed_orders_from_file(self, precomputed_orders_file):

        precomputed_orders_df = pd.read_csv(precomputed_orders_file)
        precomputed_orders_df['timestamp'] = pd.to_datetime(precomputed_orders_df['timestamp'])
        precomputed_orders_df['timestamp_minutes'] = (precomputed_orders_df['timestamp'] - precomputed_orders_df['timestamp'][0]).apply(lambda x: x.total_seconds())
        self.estimated_lam = 1 / precomputed_orders_df['timestamp_minutes'].diff().mean()
        self.average_distance = calculate_average_distance(precomputed_orders_df[['lat', 'lon']])
        self.precomputed_orders = [
            Order(i, (row['lon'], row['lat']), row['timestamp_minutes'], 10 / self.estimated_lam) for i, row in precomputed_orders_df.iterrows()
        ]
        

    def run(self):
        if self.precomputed_orders is None:
            order_timestamps_lst = sps.expon(scale=1/self.lam).rvs(self.n_orders).cumsum()
        else:
            order_timestamps_lst = [order.creation_time for order in self.precomputed_orders]

        for i, timestamp in tqdm(enumerate(order_timestamps_lst)):
            # if timestamp > self.duration:
            #     break

            for order in self.assigned_orders:
                if order.delivery_time <= timestamp:
                    self.delivered_orders.append(order)
                    self.assigned_orders.remove(order)

            if self.precomputed_orders is None:
                while True:
                    long = random.random() * (self.long_lim[1] - self.long_lim[0]) + self.long_lim[0]
                    lat = random.random() * (self.lat_lim[1] - self.lat_lim[0]) + self.lat_lim[0]
                    if is_point_in_polygon(lat, lon, self.polygon_boarders):
                        break
                new_orders = [Order(i, (long, lat), timestamp, 10 / self.lam)]
            else:
                new_orders = [self.precomputed_orders[i]]
                
            self.all_orders.extend(new_orders)
            self.waiting_orders.extend(new_orders)

            for courier in self.couriers:
                courier.update_status_and_orders(timestamp)

            for courier in self.couriers:

                if courier.status == "free":
                    courier.route_coordinates = []
                    if self.selection_algorithm == 'greedy':
                        orders_to_assign = greedy_order_selection(courier, self.waiting_orders)
                    elif self.selection_algorithm == 'annealing':
                        orders_to_assign = simulated_annealing_order_selection(courier, self.waiting_orders)
                    elif self.selection_algorithm == 'genetic':
                        orders_to_assign = genetic_algorithm_order_selection(courier, self.waiting_orders)
                    elif self.selection_algorithm[:12] == 'first_orders':
                        orders_to_assign = get_first_n_orders(courier, self.waiting_orders, int(self.selection_algorithm[-1:]))
                    elif self.selection_algorithm[:11] == 'wait_orders':
                        orders_to_assign = wait_for_n_orders(courier, self.waiting_orders, int(self.selection_algorithm[-1:]))
                    elif self.selection_algorithm == 'mean_orders':
                        orders_to_assign = mean_orders(self.couriers, self.waiting_orders)
                    else:
                        orders_to_assign = self.waiting_orders[:courier.max_orders]

                    courier.assign_orders(orders_to_assign, timestamp)
                    self.assigned_orders.extend(orders_to_assign)
                    self.waiting_orders = [order for order in self.waiting_orders if order not in orders_to_assign]

            self.states_log.append({
                'assigned_orders': self.assigned_orders.copy(),
                'waiting_orders': self.waiting_orders.copy(),
                'delivered_orders': self.delivered_orders.copy(),
                'courier_routes': {courier.id: courier.route_coordinates.copy() for courier in self.couriers},
                'courier_orders': {courier.id: [order.id for order in courier.assigned_orders.copy()] for courier in self.couriers},
                'timestamp': timestamp
            })

        total_orders_number = len(self.all_orders)
        undelivered_orders_number = 0
        least_possible_penalty = 0 
        for order in self.all_orders:
            order.calculate_penalty()
            self.total_penalty += order.penalty
            undelivered_orders_number += (1 - order.is_delivered)
            least_possible_penalty += order.least_possible_penalty

        return self.total_penalty, total_orders_number, undelivered_orders_number, least_possible_penalty

    def run_real_time(self):

        start_datetime = None

        i = 0

        while True:
            # if timestamp > self.duration:
            #     break
            print('Enter location')
            inp = input()
            if inp == 'stop':
                break

            
            lat, long = map(float, inp.split())

            cur_timestamp = datetime.now()
            if start_datetime is None:
                start_datetime = cur_timestamp
                cur_timestamp = 0
            else:
                cur_timestamp = (cur_timestamp - start_datetime).total_seconds() 

            timestamp = cur_timestamp
            
            for order in self.assigned_orders:
                if order.delivery_time <= timestamp:
                    self.delivered_orders.append(order)
                    self.assigned_orders.remove(order)

                
            new_orders = [Order(i, (long, lat), timestamp, 0, start_datetime)]
    
                
            self.all_orders.extend(new_orders)
            self.waiting_orders.extend(new_orders)

            for courier in self.couriers:
                courier.update_status_and_orders(timestamp)

            for courier in self.couriers:

                if courier.status == "free":
                    courier.route_coordinates = []
                    if self.selection_algorithm == 'greedy':
                        orders_to_assign = greedy_order_selection(courier, self.waiting_orders)
                    elif self.selection_algorithm == 'annealing':
                        orders_to_assign = simulated_annealing_order_selection(courier, self.waiting_orders)
                    elif self.selection_algorithm == 'genetic':
                        orders_to_assign = genetic_algorithm_order_selection(courier, self.waiting_orders)
                    elif self.selection_algorithm[:12] == 'first_orders':
                        orders_to_assign = get_first_n_orders(courier, self.waiting_orders, int(self.selection_algorithm[-1:]))
                    elif self.selection_algorithm[:11] == 'wait_orders':
                        orders_to_assign = wait_for_n_orders(courier, self.waiting_orders, int(self.selection_algorithm[-1:]))
                    elif self.selection_algorithm == 'mean_orders':
                        orders_to_assign = mean_orders(self.couriers, self.waiting_orders)
                    else:
                        orders_to_assign = self.waiting_orders[:courier.max_orders]

                    courier.assign_orders(orders_to_assign, timestamp)
                    self.assigned_orders.extend(orders_to_assign)
                    self.waiting_orders = [order for order in self.waiting_orders if order not in orders_to_assign]

            self.states_log.append({
                'assigned_orders': self.assigned_orders.copy(),
                'waiting_orders': self.waiting_orders.copy(),
                'delivered_orders': self.delivered_orders.copy(),
                'courier_routes': {courier.id: courier.route_coordinates.copy() for courier in self.couriers},
                'courier_orders': {courier.id: [order.id for order in courier.assigned_orders.copy()] for courier in self.couriers},
                'timestamp': timestamp
            })

            log = self.states_log[-1]
            print(i)
            print('timestamp: ', log['timestamp']) 
            print('assigned orders: ', log['assigned_orders'])
            print('waiting_orders: ', log['waiting_orders'])
            print('delivered_orders: ', log['delivered_orders'])
            print('courier_orders: ', log['courier_orders'])
            print('')

            i += 1 

        total_orders_number = len(self.all_orders)
        undelivered_orders_number = 0
        least_possible_penalty = 0 
        for order in self.all_orders:
            order.calculate_penalty()
            self.total_penalty += order.penalty
            undelivered_orders_number += (1 - order.is_delivered)
            least_possible_penalty += order.least_possible_penalty

        return self.total_penalty, total_orders_number, undelivered_orders_number, least_possible_penalty


def greedy_order_selection(courier, waiting_orders):
    selected_orders = []
    current_location = courier.location

    remaining_orders = waiting_orders.copy()

    while remaining_orders and len(selected_orders) < courier.max_orders:
        next_order = min(remaining_orders, key=lambda order: calculate_duration(current_location, order.destination_location))
        remaining_orders.remove(next_order)
        selected_orders.append(next_order)
        current_location = next_order.destination_location

    return selected_orders

def calculate_duration(location1, location2):
    _, duration = Courier.calculate_duration_and_nodes([location1, location2])
    return duration


def simulated_annealing_order_selection(courier, waiting_orders):
    if not waiting_orders:
        return []

    def calculate_total_duration(orders):
        current_location = courier.location
        total_duration = 0
        for order in orders:
            duration = calculate_duration(current_location, order.destination_location)
            total_duration += duration
            current_location = order.destination_location
        return total_duration

    def swap_orders(orders):
        if len(orders) < 2:
            return orders
        new_orders = orders[:]
        i, j = random.sample(range(len(new_orders)), 2)
        new_orders[i], new_orders[j] = new_orders[j], new_orders[i]
        return new_orders

    current_orders = random.sample(waiting_orders, min(len(waiting_orders), courier.max_orders))
    current_duration = calculate_total_duration(current_orders)
    best_orders = current_orders
    best_duration = current_duration
    temperature = 100.0
    cooling_rate = 0.5

    while temperature > 1:
        new_orders = swap_orders(current_orders)
        new_duration = calculate_total_duration(new_orders)

        if new_duration < current_duration or random.uniform(0, 1) < np.exp((current_duration - new_duration) / temperature):
            current_orders = new_orders
            current_duration = new_duration

            if new_duration < best_duration:
                best_orders = new_orders
                best_duration = new_duration

        temperature *= cooling_rate

    return best_orders


def genetic_algorithm_order_selection(courier, waiting_orders, population_size=50, generations=5, mutation_rate=0.1):
    import random

    if not waiting_orders:
        return []

    def calculate_total_duration(orders):
        current_location = courier.location
        total_duration = 0
        for order in orders:
            duration = calculate_duration(current_location, order.destination_location)
            total_duration += duration
            current_location = order.destination_location
        return total_duration

    def fitness(orders):
        return 1 / calculate_total_duration(orders)

    def mutate(orders):
        new_orders = orders[:]
        if len(new_orders) > 1:
            i, j = random.sample(range(len(new_orders)), 2)
            new_orders[i], new_orders[j] = new_orders[j], new_orders[i]
        return new_orders

    # Ensure the population size does not exceed the number of possible orders
    population_size = min(population_size, len(waiting_orders))

    # Create initial population
    population = [random.sample(waiting_orders, min(len(waiting_orders), courier.max_orders)) for _ in range(population_size)]

    for _ in range(generations):
        population = sorted(population, key=fitness, reverse=True)
        next_population = population[:population_size // 2]

        while len(next_population) < population_size:
            if len(next_population) < 2:
                next_population.extend(population[:2 - len(next_population)])
                continue
            parents = random.sample(next_population, 2)
            if len(parents[0]) > 1 and len(parents[1]) > 1:
                crossover_point = random.randint(1, min(len(parents[0]), len(parents[1])) - 1)
                child = parents[0][:crossover_point] + [order for order in parents[1] if order not in parents[0][:crossover_point]]
                if random.uniform(0, 1) < mutation_rate:
                    child = mutate(child)
                next_population.append(child)
            else:
                next_population.append(parents[0] if random.uniform(0, 1) < 0.5 else parents[1])

        population = next_population

    best_orders = max(population, key=fitness)
    return best_orders

def get_first_n_orders(courier, waiting_orders, n_orders):
    return waiting_orders[:n_orders]

def wait_for_n_orders(courier, waiting_orders, n_orders):
    if len(waiting_orders) < n_orders:
        return []
    else:
        return waiting_orders[:n_orders]


def mean_orders(couriers, waiting_orders):
    free_couriers_count = 0
    courier_max_orders = couriers[0].max_orders
    for courier in couriers:
        free_couriers_count += (courier.status == 'free')
    n_orders = min(courier_max_orders, (len(waiting_orders) + free_couriers_count - 1) // free_couriers_count)
    return waiting_orders[:n_orders]