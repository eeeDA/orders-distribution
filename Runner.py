from argparse import ArgumentParser

from Simulation import Simulation
from Plotter import prepare_data, plot_states_log

if __name__ == '__main__':

    argument_parser = ArgumentParser(description='Simulation run')
    argument_parser.add_argument('-bf', '--boarders_file', type=str,
                                 help='File with boarder coordinates')
    argument_parser.add_argument('-pof', '--precomputed_orders', type=str,
                                 help='File with precomputed_orders')
    argument_parser.add_argument('-lam', '--lambda_par', type=str,
                                 help='Parameter lambda to sample orders')
    argument_parser.add_argument('-n', '--n_orders', type=str,
                                 help='Number of orders to sample')
    argument_parser.add_argument('-ss', '--sample_similar', type=bool,
                                 help='Flag if user wants to sample similar to precomputed orders')
    argument_parser.add_argument('-rr', '--run_real_time', action='store_true',
                                 help='Flag if run real time simulation')
    args = argument_parser.parse_args()

    simulation = Simulation(
        polygon_boarders_file=args.boarders_file, 
        precomputed_orders_file=args.precomputed_orders, 
        n_orders=args.n_orders,
        lam=args.lambda_par,
        sample_similar=args.sample_similar,
    )

    if not args.run_real_time:
        states_log = simulation.run()
    else:
        states_log = simulation.run_real_time()

    print(states_log)